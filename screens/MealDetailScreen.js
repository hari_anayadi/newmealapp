import { StyleSheet, Text, View, Image, Button } from "react-native";
import React, { useLayoutEffect } from "react";
import { MEALS } from "../data/dummy-data";
import MealDetailComponent from "../components/MealDetailComponent";
import IconButton from "../components/IconButton";

const MealDetailScreen = ({ navigation, route }) => {
  const mealId = route.params.MealId;
  const SelectedMeal = MEALS.find((meal) => meal.id === mealId);
  function HeaderButtonPressHandler() {
    console.log("pressed");
  }
  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => {
        return(
        <IconButton
        icon={'star'}
        color={'white'}
         onPress={HeaderButtonPressHandler} />)
      },
    });
  }, [navigation,HeaderButtonPressHandler]);
  return (
    <View>
      <Image
        source={{ uri: SelectedMeal.imageUrl }}
        style={styles.Imagestyle}
      />
      <Text style={[styles.TextStyle, { fontSize: 26, fontWeight: "bold" }]}>
        {SelectedMeal.title}
      </Text>
      <MealDetailComponent
        duration={SelectedMeal.duration}
        complexity={SelectedMeal.complexity}
        affordability={SelectedMeal.affordability}
      />
      <View></View>
      <Text style={[styles.TextStyle, { fontSize: 20, color: "red" }]}>
        Ingredients
      </Text>
      {SelectedMeal.ingredients.map((ingredient) => (
        <Text key={ingredient} style={[styles.TextStyle, { color: "blue" }]}>
          {ingredient}
        </Text>
      ))}
      <Text style={[styles.TextStyle, { fontSize: 20, color: "red" }]}>
        Steps
      </Text>
      {SelectedMeal.steps.map((steps) => (
        <Text key={steps} style={[styles.TextStyle, { color: "green" }]}>
          {steps}
        </Text>
      ))}
    </View>
  );
};

export default MealDetailScreen;

const styles = StyleSheet.create({
  TextStyle: {
    color: "white",
    textAlign: "center",
  },
  Imagestyle: {
    height: 250,
    width: "100%",
  },
});
