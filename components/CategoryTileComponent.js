import {
  StyleSheet,
  Text,
  View,
  Pressable,
  useWindowDimensions,
} from "react-native";
import React from "react";

const CategoryTileComponent = ({ title, color, onPress }) => {
  const { height, width } = useWindowDimensions();
  return (
    <View style={[styles.GridContainer, { backgroundColor: color }]}>
      <Pressable
        android_ripple={{ color: "red" }}
        style={({ pressed }) => [
          styles.button,
          pressed ? styles.buttonPressed : null,
        ]}
        onPress={onPress}
      >
        <View style={[styles.InnerContainer]}>
          <Text style={styles.InnerText}>{title}</Text>
        </View>
      </Pressable>
    </View>
  );
};

export default CategoryTileComponent;
//fios="for ios"
const styles = StyleSheet.create({
  GridContainer: {
    flex: 1,
    margin: 16,
    height: 150,
    borderRadius: 8,
    elevation: 5,
    shadowOpacity: 0.25, //fios
    shadowOffset: { width: 0, height: 2 }, //fios
    shadowRadius: 8, //fios
    shadowColor: "black", //fios
    overflow: Platform.OS === "android" ? "hidden" : "visible",
  },
  button: {
    flex: 1,
  },
  InnerContainer: {
    flex: 1,
    padding: 16,
    alignItems: "center",
    justifyContent: "center",
  },
  InnerText: {
    fontWeight: "bold",
    fontSize: 18,
  },
  buttonPressed: {
    //fios(like android-ripple)
    opacity: 0.5,
  },
});
