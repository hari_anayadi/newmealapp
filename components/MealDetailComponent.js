import { StyleSheet, Text, View } from "react-native";
import React from "react";

const MealDetailComponent = ({duration,complexity,affordability}) => {
  return (
    <View style={styles.DetailsContainer}>
      <Text style={styles.DetailItem}>{duration} Miniuts</Text>
      <Text style={styles.DetailItem}>{complexity.toUpperCase()}</Text>
      <Text style={styles.DetailItem}>{affordability.toUpperCase()}</Text>
    </View>
  );
};

export default MealDetailComponent;

const styles = StyleSheet.create({
  DetailsContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    padding: 8,
  },
  DetailItem: {
    marginHorizontal: 5,
    color:"red",
    fontWeight:"bold",
    fontSize:15
  },
});
