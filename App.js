import { StyleSheet,Button,Text } from "react-native";
import React from "react";
import CategoryScreen from "./screens/CategoryScreen";
import MealsOverView from "./screens/MealsOverView";
import { StatusBar } from "expo-status-bar";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import MealDetailScreen from "./screens/MealDetailScreen";
const App = () => {
  const Stack = createNativeStackNavigator();
  return (
    <>
      <StatusBar hidden />
      <NavigationContainer>
        <Stack.Navigator screenOptions={{
           headerStyle: { backgroundColor: "black" },
           headerTintColor:"white",
           contentStyle:{backgroundColor:"black"}
        }}>
          <Stack.Screen
            name="MealsCategory"
            component={CategoryScreen}
            options={{
              title: " All Categories",
            }}
          />
          <Stack.Screen
           name="MealsOverView"
            component={MealsOverView}
            options={({route,navigation})=>{
              const CatId=route.params.CategoryId;
              return{
                title:CatId
              }
            }} />
            <Stack.Screen name="MealDetail" 
            component={MealDetailScreen}
            options={{
              headerRight:()=>{
                return(
                  <Button title="Tap me!"/>
                )
              }
            }}/>
        </Stack.Navigator>
      </NavigationContainer>
    </>
  );
};

export default App;

const styles = StyleSheet.create({});
