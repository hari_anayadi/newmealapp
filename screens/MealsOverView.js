import { StyleSheet, Text, View, FlatList } from "react-native";
import React,{useEffect,useLayoutEffect } from "react";
import { CATEGORIES, MEALS } from "../data/dummy-data";
import MealItem from "../components/MealItem";

const MealsOverView = ({route,navigation}) => {
  
  const CatID = route.params.CategoryId;
  // console.log(CatID);
  const DisplayedMeals = MEALS.filter((mealIem) => {
    return mealIem.categoryIds.indexOf(CatID) >= 0;
  });
  useLayoutEffect(() => {
    const CategoryTitle=CATEGORIES.find((category)=>category.id=== CatID).title
    navigation.setOptions({
      title:CategoryTitle
    })
  }, [CatID,navigation]);
 
  // console.log(DisplayedMeals)
  function RenderCategoryItem(itemData) {
    const item = itemData.item;
    //important item=itemData.item
    const MealItemProps = {
      title: item.title,
      ImageUrl: item.imageUrl,
      duration: item.duration,
      complexity: item.complexity,
      affordability: item.affordability,
      id:item.id
    };
    return <MealItem {...MealItemProps} />; 
    //using dot operator
  }
  return (
    <View style={styles.container}>
      <FlatList
        data={DisplayedMeals}
        keyExtractor={(item) => item.id}
        renderItem={RenderCategoryItem}
      />
    </View>
  );
};

export default MealsOverView;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
  },
});
