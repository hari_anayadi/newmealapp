import { StyleSheet, Text, View, FlatList } from "react-native";
import React from "react";
import { CATEGORIES } from "../data/dummy-data";
import CategoryTileComponent from "../components/CategoryTileComponent";
import { useNavigation } from "@react-navigation/native";

const CategoryScreen = () => {
  const navigation=useNavigation()
  function RenderedItem(itemData) {
    function PressHandler()
    {
        navigation.navigate('MealsOverView',{CategoryId:itemData.item.id})
    }

    return (
      <CategoryTileComponent
        title={itemData.item.title}
        color={itemData.item.color}
        onPress={PressHandler}
      />
    );
  }
  return (
    <View>
      <FlatList
        data={CATEGORIES}
        keyExtractor={(item) => item.id}
        renderItem={RenderedItem}
        numColumns={2}
      />
    </View>
  );
};

export default CategoryScreen;

const styles = StyleSheet.create({});
