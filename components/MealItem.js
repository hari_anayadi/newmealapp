import { StyleSheet, Text, View, Image, Pressable } from "react-native";
import React from "react";
import { useNavigation } from "@react-navigation/native";
import MealDetailComponent from "./MealDetailComponent";
const MealItem = ({
  title,
  ImageUrl,
  duration,
  complexity,
  affordability,
  id,
}) => {
  const navigation = useNavigation();
  function SelectMealIemHandler() {
    navigation.navigate("MealDetail",{MealId:id});
  }
  return (
    <View style={styles.MealItemView}>
      <Pressable
        android_ripple={{ color: "cyan" }}
        onPress={SelectMealIemHandler}
      >
        <Image source={{ uri: ImageUrl }} style={styles.ImageStyle} />
        <Text style={styles.TitleFontStyle}>{title}</Text>
        <MealDetailComponent duration={duration} complexity={complexity} affordability={affordability}/>
      </Pressable>
    </View>
  );
};

export default MealItem;

const styles = StyleSheet.create({
  ImageStyle: {
    width: "100%",
    height: 200,
  },
  TitleFontStyle: {
    fontWeight: "bold",
    fontSize: 20,
    textAlign: "center",
  },
  
  MealItemView: {
    backgroundColor: "white",
    padding: 10,
    elevation: 10,
    marginVertical:10
  },
});
